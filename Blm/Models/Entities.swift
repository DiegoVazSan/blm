//
//  Entities.swift
//  Blm
//
//  Created by Diego on 04/06/22.
//
class ResponseEntitie : Codable {
        var meta: MetaEntitie?
        var data: [EmployeeEntitie]?
}

class MetaEntitie : Codable {
    var pagination: PaginationEntitie?
}

class PaginationEntitie : Codable {
    var total: Int?
    var pages: Int?
    var page: Int?
    var limit: Int?
    var links: LinkSEntitie?
}

class LinkSEntitie : Codable {
    var previous: String?
    var current: String?
    var next: String?
}

class EmployeeEntitie : Codable {
    var id: Int?
    var name: String?
    var email: String?
    var gender: String?
    var status: String?
}

