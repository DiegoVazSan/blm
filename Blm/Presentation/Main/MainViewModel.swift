//
//  MainViewModel.swift
//  Blm
//
//  Created by Diego on 04/06/22.
//

import UIKit

class BtnAtrributes {
    var bckGroundColor: UIColor?
    var txtBtn: String?
    
    init(color: UIColor, text: String){
        bckGroundColor = color
        txtBtn = text
    }
}

class MainViewModel {
//    MARK: -Properties
    var modelData = [BtnAtrributes(color: .blue , text: "Abrir Lector QR" ),
                 BtnAtrributes(color: .red, text: "Ver Colaboradores")]
    
//    MARK: -Functions
//    MARK: Table Properties
    func getNumberOfRows() -> Int { modelData.count }
    
    func getCell(tbl: UITableView, idx: IndexPath) -> UITableViewCell {
        let cell = tbl.dequeueReusableCell(withIdentifier: MainTblCell.identifierCell, for: idx) as! MainTblCell
        let item = modelData[idx.row]
        cell.configure(with: item)
        return cell
    }
    
    func actionAfterTouchingCell(tbl: UITableView, idx: IndexPath){
        tbl.deselectRow(at: idx, animated: true)
        print("You did tap cell")
    }
    
    func assignHeightCell() -> CGFloat { 150 }
}
