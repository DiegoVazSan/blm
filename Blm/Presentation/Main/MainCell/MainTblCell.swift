//
//  MainTblCell.swift
//  Blm
//
//  Created by Diego on 04/06/22.
//

import UIKit

protocol MainTblCellDelegate : AnyObject {
    func didTapBtn(cell: MainTblCell)
}

class MainTblCell: UITableViewCell {
    // MARK: - Outlets
    
    @IBOutlet weak var btnAction: UIButton!
    
    // MARK: - Propiedades
    
    static let identifierCell = "MainTableViewCell"
    static let nibName = "MainTblCell"
    weak var delegate: MainTblCellDelegate?
    
    // MARK: - Ciclo de Vida
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    // MARK: - Navegación
    
    // MARK: - IBActions
    
    @IBAction func actionBtn(_ sender: Any) {
        delegate?.didTapBtn(cell: self)
    }
    // MARK: - Funciones
    
    static func registerCell(tableView:UITableView){
        let nib = UINib(nibName: MainTblCell.nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: MainTblCell.identifierCell)
    }
    
    func configure(with item: BtnAtrributes){
        btnAction.setTitle(item.txtBtn, for: .normal)
        btnAction.setTitleColor(.white, for: .normal)
        btnAction.backgroundColor = item.bckGroundColor
    }
    
    func setupUI(){
        btnAction.setShadow()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
