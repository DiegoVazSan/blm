//
//  MainTableVC.swift
//  Blm
//
//  Created by Diego on 03/06/22.
//

import UIKit

class MainTableVC: UITableViewController {
    
    // MARK: - Propiedades
    let viewModel = MainViewModel()
    var coordinator: MainCoordinator?
    
    // MARK: - Ciclo de Vida

    override func viewDidLoad() {
        super.viewDidLoad()
        MainTblCell.registerCell(tableView: self.tableView)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    // MARK: - Funciones
    // MARK:  TABLE FUNCTIONS

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  viewModel.getCell(tbl: self.tableView, idx: indexPath) as! MainTblCell
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.actionAfterTouchingCell(tbl: self.tableView, idx: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.assignHeightCell()
    }
    
}

extension MainTableVC : MainTblCellDelegate {
    func didTapBtn(cell: MainTblCell) {
        let idx = self.tableView.indexPath(for: cell)
        idx?.row == 0 ? coordinator?.showQRReader() : coordinator?.showWorkerList()
        
    }
}
