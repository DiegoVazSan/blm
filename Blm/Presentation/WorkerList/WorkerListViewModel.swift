//
//  WorkerListViewModel.swift
//  Blm
//
//  Created by Diego on 04/06/22.
//

import Alamofire
import UIKit

class WorkerListViewModel {
    // MARK: - Propiedades
    let url: String = "https://gorest.co.in/public/v1/users"
    var data = [EmployeeEntitie]()
    var coordinator: MainCoordinator?
    
    // MARK: - Funciones
    func loadData(handler: @escaping () -> Void ){
        AF.request(self.url, method: .get).responseDecodable(of: ResponseEntitie.self) { [weak self] response in
            if let datos = response.value?.data {
                self?.data = datos
                handler()
            }
        }
    }
    
    
    // MARK: TABLE FUNCTIONS
    func getNumberOfRows() -> Int { data.count }
    
    func getHeightForRow() -> CGFloat { 100 }
    
    func didTapCell(tbl: UITableView, idx: IndexPath){
        tbl.deselectRow(at: idx, animated: true)
        print("You did tap cell \(idx.row)")
        coordinator?.showWorkerDetail(of: data[idx.row])
    }
    
    func getCell(tbl: UITableView, idx: IndexPath) -> UITableViewCell {
        let cell = tbl.dequeueReusableCell(withIdentifier: WorkerTblCell.identifierCell, for: idx) as! WorkerTblCell
        let item = data[idx.row]
        cell.accessoryType = .disclosureIndicator
        cell.configure(with: item)
        return cell
    }
}
