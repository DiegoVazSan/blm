//
//  WorkerTblCell.swift
//  Blm
//
//  Created by Diego on 04/06/22.
//

import UIKit

class WorkerTblCell: UITableViewCell {
    
    // MARK: - Propiedades
    
    static let identifierCell = "WorkerTableViewCell"
    static let nibName = "WorkerTblCell"
    
    // MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    

    // MARK: - Ciclo de Vida
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - Metdos
    
    static func registerCell(tableView:UITableView){
        let nib = UINib(nibName: WorkerTblCell.nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: WorkerTblCell.identifierCell)
    }
    
    func configure(with item: EmployeeEntitie){
        lblTitle.text = item.name
        lblSubTitle.text = item.email
    }
    
}
