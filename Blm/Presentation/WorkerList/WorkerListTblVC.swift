//
//  WorkerListTblVC.swift
//  Blm
//
//  Created by Diego on 04/06/22.
//

import UIKit

class WorkerListTblVC: UITableViewController {
    // MARK: - Propiedades
    var viewModel = WorkerListViewModel()
    
    // MARK: - Ciclo de Vida
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Colaboradores"
        setupTbl()
    }

    // MARK: - Funciones
    
    func setupTbl(){
        WorkerTblCell.registerCell(tableView: self.tableView)
        viewModel.loadData {
            self.tableView.reloadData()
        }
    }

    // MARK: - Table view functions
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.getCell(tbl: self.tableView, idx: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getHeightForRow()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didTapCell(tbl: self.tableView, idx: indexPath)
    }
    
}
