//
//  WorkerDetailVC.swift
//  Blm
//
//  Created by Diego on 05/06/22.
//

import UIKit

class WorkerDetailVC: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var imgViewWorker: UIImageView!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    
    // MARK: - Propiedades
    var viewModel = WorkerDetailViewModel()
    
    // MARK: - Ciclo de Vida
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configureView()
    }
    
    // MARK: - Navegación
    
    
    
    // MARK: - IBActions
    
    
    
    // MARK: - Funciones
    
    func setupUI(){
        lblGender.layer.cornerRadius = 10
        lblGender.layer.masksToBounds = true
        lblStatus.layer.cornerRadius = 10
        lblStatus.layer.masksToBounds = true
    }
    
    func configureView(){
        let employee = viewModel.employee
        
        if employee?.gender == "female" {
            imgViewWorker.image = UIImage(named: "femaleIC")
            
        } else {
            imgViewWorker.image = UIImage(named: "maleIC")
            lblGender.backgroundColor = .blue
        }
        
        if employee?.status == "inactive" {
            print("nothing")
        }  else {
            lblStatus.backgroundColor = .green
        }
        
        if let id = employee?.id { lblID.text = "\(id)" }
        lblName.text = employee?.name
        lblEmail.text = employee?.email
        lblGender.text = employee?.gender
        lblStatus.text = employee?.status
    }
}
