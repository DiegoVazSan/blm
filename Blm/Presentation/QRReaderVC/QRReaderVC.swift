//
//  QRReaderVC.swift
//  Blm
//
//  Created by Diego on 06/06/22.
//

import UIKit

class QRReaderVC: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var btnOpenQR: UIButton!
    @IBOutlet weak var txtView: UITextView!
    
    
    
    // MARK: - Propiedades
    var viewModel = QRReaderViewModel()
    var coordinator = MainCoordinator()
    
    // MARK: - Ciclo de Vida
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - IBActions
    @IBAction func openQR(_ sender: Any) {
        print("Abrir codigo QR")
    }
    
    
    
    // MARK: - Funciones
    func setupUI(){
        self.title = "Lector QR"
        btnOpenQR.setShadow()
    }
    
}
