//
//  Coordinator.swift
//  Blm
//
//  Created by Diego on 04/06/22.
//

 
import UIKit

protocol Coordinator {
    var navigationController: UINavigationController { set get }
    func start()
}
