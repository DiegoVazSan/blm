//
//  MainCoordinator.swift
//  Blm
//
//  Created by Diego on 04/06/22.
//

import Foundation
import UIKit

class MainCoordinator : Coordinator {
    var navigationController: UINavigationController = UINavigationController()
    
    func start() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MainTableVC") as! MainTableVC
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func showWorkerList(){
        let vc = WorkerListTblVC(nibName: "WorkerListTblVC", bundle: nil)
        vc.viewModel.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func showWorkerDetail(of item: EmployeeEntitie){
        let vc = WorkerDetailVC(nibName: "WorkerDetailVC", bundle: nil)
        vc.viewModel.employee = item
        vc.viewModel.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func showQRReader(){
        let vc = QRReaderVC(nibName: "QRReaderVC", bundle: nil)
        vc.viewModel.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
}
