//
//  UIViewExtentions.swift
//  Blm
//
//  Created by Diego on 04/06/22.
//

import UIKit


extension UIButton {
    
    func setShadow() {
        // Shadow Color and Radius
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.40).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        self.layer.shadowOpacity = 5.0
        self.layer.shadowRadius = 0.0
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 15.0
      }
}
